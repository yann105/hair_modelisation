%Euler
current

Nt=700;
dt=800;
Cb=0;

%Calcul de u_new et v_new 
u_new=zeros(Nx+1,Ny);
v_new=zeros(Nx,Ny+1);
for i=2:Nx
    u_new(i,:)=(u(i-1,:)+u(i,:))/2;
end
u_new(Nx+1,:)=u_new(Nx,:);
u_new(1,:)=u_new(2,:);
for j=2:Ny
    v_new(:,j)=(v(:,j-1)+v(:,j))/2;
end
v_new(:,Ny+1)=v_new(:,Ny);
v_new(:,1)=v_new(:,2);
%Condition initiale pour C
C=zeros(Nx,Ny,Nt);
C(150:160,70:80,1)=1;
put=zeros(Nx,Ny);
put(46,107)=1;


%Boucle pour calculer les F 
for l=1:Nt
    for i=1:Nx
        for j=1:Ny
            if u_new(i,j)>=0
                if i==1
                Fx1=u_new(i,j)*Cb;
                else
                Fx1=u_new(i,j)*C(i-1,j,l);
                end
            else
                Fx1=u_new(i,j)*C(i,j,l);
            end
            
            if u_new(i+1,j)>=0
                Fx2=-u_new(i+1,j)*C(i,j,l);
            else
               if i == Nx
                Fx2=-u_new(i+1,j)*Cb;
                
                else
                Fx2=-u_new(i+1,j)*C(i+1,j,l);
               end
            end
            if v_new(i,j)>=0
                if j==1
                 Fy1=v_new(i,j)*Cb;
                else
                Fy1=v_new(i,j)*C(i,j-1,l);
                end
            else
                Fy1=v_new(i,j)*C(i,j,l);
            end
            if v_new(i,j+1)>=0
                Fy2=-v_new(i,j+1)*C(i,j,l);
            else
                if j == Ny
                Fy2=-v_new(i,j+1)*Cb;
                else 
                Fy2=-v_new(i,j+1)*C(i,j+1,l);
                end
            end
            C(i,j,l+1)=C(i,j,l)+dt*(Fx1/dx+Fx2/dx+Fy1/dy+Fy2/dy)+put(i,j);
        end
    end
end


figure(2)
pcolor((C(:,:,450)./mask.*mask)');colorbar
hold on
quiver(x(5:5:Nx),y(5:5:Ny),u_graph',v_graph','w')
hold off




