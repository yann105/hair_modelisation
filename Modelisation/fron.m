euler

    CT(1)=sum(sum(C(:,:,1)))*dx*dy;
    dCTdt(1)=0;
for l=2:Nt
    CT(l)=sum(sum(C(:,:,l)))*dx*dy;
    dCTdt(l)=(CT(l)-CT(l-1))/dt;
end

for l=1:Nt
    for j=1:Ny
          if u_new(1,j)>=0 %Ouest
          X0(j,l)=u_new(1,j)*Cb*dy;
          else
          X0(j,l)=u_new(1,j)*C(1,j,l)*dy;
          end
          
          if u_new(Nx+1,j)>=0 %Est
          XF(j,l)=-u_new(Nx+1,j)*C(Nx,j,l)*dy;
          else
          XF(j,l)=-u_new(Nx+1,j)*Cb*dy;
          end
    end
    
    for i=1:Nx
        if v_new(i,1)>=0 %Sud
         Y0(i,l)=v_new(i,1)*Cb*dx;
        else
         Y0(i,l)=v_new(i,1)*C(i,1,l)*dx;
        end
         if v_new(i,Ny+1)>=0 %Nord
        YF(i,l)=-v_new(i,Ny+1)*C(i,Ny,l)*dx;
         else       
        YF(i,l)=-v_new(i,Ny+1)*Cb*dx;
         end
end
end
figure(3)
plot(CT)

Flux_EO=sum(X0+XF);%frontiere Est-Ouest
Flux_SN=sum(Y0+YF);%frontiere Sud-Nord
Flux_total=Flux_EO+Flux_SN+sum(sum(put))*dx*dy/dt;
figure(4)
plot(Flux_total)

figure(5)
plot(dCTdt)

figure(6)

plot(dCTdt(2:Nt),Flux_total(1:Nt-1),'+')